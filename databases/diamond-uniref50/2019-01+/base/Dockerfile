FROM ubuntu:20.04

ARG version

LABEL maintainer="mmp@uit.no" \
  dbVersion=$version \
  dbName=diamond-UniProt \
  url=http://ftp.ebi.ac.uk/pub/databases/uniprot

COPY --from=registry.gitlab.com/uit-sfb/genomic-tools/diamond:2.0.15 /app/diamond /app/diamond

WORKDIR /db/uniprot/uniref50

RUN apt update -y && apt install wget -y

#This code only works for the latest release. So when grep <version> fails in the metalink file,
#it is time to move this version into the 2019-01+ and bump up the version both in this dlp.conf.yaml
#and this directory version
RUN V=$(echo $version | tr - _) \
&& wget -q http://ftp.ebi.ac.uk/pub/databases/uniprot/current_release/uniref/uniref50/RELEASE.metalink \
&& echo version=$V \
&& grep $V RELEASE.metalink \
&& wget -q http://ftp.ebi.ac.uk/pub/databases/uniprot/current_release/uniref/uniref50/uniref50.fasta.gz

WORKDIR /db/diamond/uniprot/uniref50

RUN ["/app/diamond/diamond", "makedb", "--in", "/db/uniprot/uniref50/uniref50.fasta.gz", "-d", "nr"]

ENTRYPOINT []


#Here we need to keep the multi-build otherwise the end image would have both the original db and the processed db
#FROM alpine@sha256:644fcb1a676b5165371437feaa922943aaf7afcfa8bfee4472f6860aad1ef2a0

#WORKDIR /db/diamond/uniprot/uniref50

#COPY --from=builder ["/db/uniprot/uniref50/*.sqlite3","/db/diamond/uniprot/uniref50/*.dmnd","./"]
