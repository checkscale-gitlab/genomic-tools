#!/usr/bin/env bash

for entry in "/db/priam/PROFILES"/*.chk
do
  echo "$entry" | tee -a profiles.list
done