# Genomic-tools


Genomic-tools gathers some Docker image of common genomic and X-omic tools:

- [BBMap](https://sourceforge.net/projects/bbmap/)
- [Blast](https://blast.ncbi.nlm.nih.gov/Blast.cgi)
- [Diamond](https://github.com/bbuchfink/diamond)
- [HMMER](http://hmmer.org/)
- [InterproScan5](https://github.com/ebi-pf-team/interproscan)
- [Kaiju](https://github.com/bioinformatics-centre/kaiju)
- [MAPseq](https://github.com/jfmrod/MAPseq)
- [MaxBin](https://sourceforge.net/projects/maxbin/)
- [MEGAHIT](https://github.com/voutcn/megahit)
- [MGA](https://github.com/crukci-bioinformatics/MGA)
- [PRIAM](http://priam.prabi.fr/)
- [SeqPrep](https://github.com/jstjohn/SeqPrep)
- [Trimmomatic](http://www.usadellab.org/cms/?page=trimmomatic)

No version should contain underscores in the name.

All the images are available [here](https://gitlab.com/uit-sfb/genomic-tools/container_registry).

Please visit our [Wiki](https://gitlab.com/uit-sfb/genomic-tools/wikis/home) for more information about this project.

## Directory structure

The project uses [docker-library-publish](https://gitlab.com/uit-sfb/docker-library-publish) to manage the project.

